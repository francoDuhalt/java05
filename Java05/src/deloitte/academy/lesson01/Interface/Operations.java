package deloitte.academy.lesson01.Interface;

import java.util.List;

import deloitte.academy.lesson01.abstractas.Products;

public interface Operations {
	
	/**
	 * Method to add an element to an arrayList
	 * @param productList
	 * @param product
	 * @return string
	 */

	public String addProduct(List<Products> productList, Products product);
	
	/**
	 * Method to remove an element from an arrayList
	 * @param productList
	 * @param id
	 * @return string
	 */
	public String removeProduct(List<Products> productList, int id);
	
	/**
	 * Method to edit the name of an element from an arrayList
	 * @param productList
	 * @param id
	 * @param name
	 * @return string
	 */
	public String editName(List<Products> productList, int id, String name);
	
	/**
	 * Method to edit the price of an element from an arrayList
	 * @param productList
	 * @param id
	 * @param price
	 * @return string
	 */
	public String editPrice(List<Products> productList, int id, double price);
	
	/**
	 * Method to edit the category of an element from an arrayList
	 * @param productList
	 * @param id
	 * @param category
	 * @return string
	 */
	public String editCategory(List<Products> productList, int id, String category);
	
	/**
	 * Method to edit the provider of an element from an arrayList
	 * @param productList
	 * @param id
	 * @param provider
	 * @return string
	 */
	public String editProvider(List<Products> productList, int id, String provider);
	
	/**
	 * Method to edit the quantity of an element from an arrayList
	 * @param productList
	 * @param id
	 * @param quantity
	 * @return string
	 */
	public String editQuantity(List<Products> productList, int id, int quantity);
	
	/**
	 * Method to edit the quantity in warehouseList and add an element to refundList
	 * @param productList
	 * @param product
	 * @param warehouseList
	 * @return string
	 */
	public String refund(List<Products> productList, Products product, List<Products> warehouseList);
	
	/**
	 * Method for warehouse movement in with two arrayList
	 * @param productList
	 * @param id
	 * @param quantity
	 * @param warehouseList
	 * @return string
	 */
	public String warehouseMovementIn(List<Products> productList, int id, int quantity, List<Products> warehouseList);
	
	/**
	 * Method for warehouse movement out with two arrayList
	 * @param productList
	 * @param id
	 * @param quantity
	 * @param warehouseList
	 * @return string
	 */
	
	public String warehouseMovementOut(List<Products> productList, int id, int quantity, List<Products> warehouseList);
	
	
}
