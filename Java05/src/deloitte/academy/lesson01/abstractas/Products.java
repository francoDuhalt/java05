package deloitte.academy.lesson01.abstractas;
/**
 * 
 * @author Franco Duhalt
 *
 */

public abstract class Products {
	public int id;
	public String name;
	public double cost;
	public String category;
	public String provider;
	public int total;
	
	
	
	public Products() {
		// TODO Auto-generated constructor stub
	}
	
	
	public Products(int id, String name, double cost, String category, String provider, int total) {
		super();
		this.id = id;
		this.name = name;
		this.cost = cost;
		this.category = category;
		this.provider = provider;
		this.total = total;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	
	
}
