package deloitte.academy.lesson01.entity;

/**
 * Type of Categories
 * @author fduhalt
 *
 */
public enum Categories {
	Laptop, Audifonos, Memorias;
}
