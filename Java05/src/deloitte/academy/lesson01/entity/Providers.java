package deloitte.academy.lesson01.entity;

/**
 * Types of providers
 * @author Franco Duhalt
 *
 */
public enum Providers {
	Lenovo, HP, Razer;
}
