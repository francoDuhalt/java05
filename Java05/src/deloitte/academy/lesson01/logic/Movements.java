package deloitte.academy.lesson01.logic;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.academy.lesson01.Interface.Operations;
import deloitte.academy.lesson01.abstractas.Products;

/**
 * Class for warehouse movements and CRUD for products
 * 
 * @author Franco Duhalt
 *
 */

public class Movements implements Operations {
	private static final Logger LOGGER = Logger.getLogger(Movements.class.getName());
	
	/**
	 * Method to add an element to an arrayList
	 */
	@Override
	public String addProduct(List<Products> productList, Products product) {
		// TODO Auto-generated method stub
		String message = "";
		try {
			productList.add(product);
			message = "Se ha agregado el producto " + product.getName();
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return message;
	}

	/**
	 * Method to remove an element from an arrayList
	 */
	@Override
	public String removeProduct(List<Products> productList, int id) {
		// TODO Auto-generated method stub
		String message = "";
		Products productEliminate = null;
		String productName = "";
		
		try {
			for (Products productID: productList) {
				if(productID.getId() == id) {
					productEliminate = productID;
					productName = productID.getName();
				}
			}
			productList.remove(productEliminate);
			message = "Se ha eliminado el producto "+ productName;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return message;
	}


	/**
	 * Method to edit the name of an element from an arrayList
	 */
	@Override
	public String editName(List<Products> productList, int id, String name) {
		// TODO Auto-generated method stub
		String message = "";
		String productName = "";
		
		try {
			for (Products productID: productList) {
				if(productID.getId() == id) {
					productID.setName(name);
					productName = productID.getName();
				}
			}
			message = "Se ha modificado el nombre del producto "+ productName;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return message;
	}


	/**
	 * Method to edit the price of an element from an arrayList
	 */
	@Override
	public String editPrice(List<Products> productList, int id, double price) {
		// TODO Auto-generated method stub
		String message = "";
		String productName = "";
		
		try {
			for (Products productID: productList) {
				if(productID.getId() == id) {
					productID.setCost(price);;
					productName = productID.getName();
				}
			}
			message = "Se ha modificado el precio del producto "+ productName;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return message;
	}


	/**
	 * Method to edit the category of an element from an arrayList
	 */
	@Override
	public String editCategory(List<Products> productList, int id, String category) {
		// TODO Auto-generated method stub
		String message = "";
		String productName = "";
		
		try {
			for (Products productID: productList) {
				if(productID.getId() == id) {
					productID.setCategory(category);;
					productName = productID.getName();
				}
			}
			message = "Se ha modificado la categoria del producto "+ productName;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return message;
	}


	/**
	 * Method to edit the provider of an element from an arrayList
	 */
	@Override
	public String editProvider(List<Products> productList, int id, String provider) {
		// TODO Auto-generated method stub
		String message = "";
		String productName = "";
		
		try {
			for (Products productID: productList) {
				if(productID.getId() == id) {
					productID.setProvider(provider);
					productName = productID.getName();
				}
			}
			message = "Se ha modificado el proveedor del producto "+ productName;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return message;
	}


	/**
	 * Method to edit the quantity of an element from an arrayList
	 */
	@Override
	public String editQuantity(List<Products> productList, int id, int quantity) {
		// TODO Auto-generated method stub
		String message = "";
		String productName = "";
		
		try {
			for (Products productID: productList) {
				if(productID.getId() == id) {
					productID.setTotal(quantity);
					productName = productID.getName();
				}
			}
			message = "Se ha modificado la cantidad del producto "+ productName;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return message;
		
	}
	/**
	 * Method to edit the quantity in warehouseList and add an element to refundList
	 */

	@Override
	public String refund(List<Products> productList, Products product, List<Products> warehouseList) {
		// TODO Auto-generated method stub
		String message = "";
		String productName = "";
		
		try {
			productList.add(product);
			for (Products productID: warehouseList) {
				if(productID.getId() == product.getId()) {
					productID.setTotal(productID.getTotal() + product.getTotal());
					productName = productID.getName();
				}
			}
			message = "Se ha hecho una devolución del producto " + productName;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return message;
	}
	 /**
	  *  Method for warehouse movement in with two arrayList
	  */
	@Override
	public String warehouseMovementIn(List<Products> productList, int id, int quantity, List<Products> warehouseList) {
		// TODO Auto-generated method stub
		String message = "";
		String productName = "";
		
		try {
			for (Products productID: productList) {
				if(productID.getId() == id) {
					productName = productID.getName();
					productID.setTotal(productID.getTotal() + quantity);
				}
			}
			for (Products productID: warehouseList) {
				if(productID.getId() == id) {
					productID.setTotal(productID.getTotal() - quantity);
				}
			}
			message = "Se ha hecho una entrada de almacen del producto " + productName;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return message;
	}
	
	/**
	 * Method for warehouse movement out with two arrayList
	 */
	@Override
	public String warehouseMovementOut(List<Products> productList, int id, int quantity, List<Products> warehouseList) {
		// TODO Auto-generated method stub
		String message = "";
		String productName = "";
		
		try {
			for (Products productID: productList) {
				if(productID.getId() == id) {
					productName = productID.getName();
					productID.setTotal(productID.getTotal() - quantity);
				}
			}
			for (Products productID: warehouseList) {
				if(productID.getId() == id) {
					productID.setTotal(productID.getTotal() + quantity);
				}
			}
			message = "Se ha hecho una salida de almacen del producto " + productName;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return message;
	}

}
