package deloitte.academy.lesson01.run;

import java.util.ArrayList;
import java.util.List;

import deloitte.academy.lesson01.abstractas.Products;
import deloitte.academy.lesson01.entity.Categories;
import deloitte.academy.lesson01.entity.ProductStore;
import deloitte.academy.lesson01.entity.ProductWarehouse;
import deloitte.academy.lesson01.entity.Providers;
import deloitte.academy.lesson01.logic.Movements;

public class RunWarehouse {
	
	public static final List<Products> store = new ArrayList<Products>();
	public static final List<Products> warehouse = new ArrayList<Products>();
	public static final List<Products> newProducts = new ArrayList<Products>();
	public static final List<Products> refund = new ArrayList<Products>();
	
	public static final ProductStore product1 = new ProductStore(1, "Laptop 300", 5000.00, Categories.Laptop.toString(), Providers.Lenovo.toString(), 50);
	
	public static final ProductWarehouse product2 = new ProductWarehouse(1, "Laptop 300", 5000.00, Categories.Laptop.toString(), Providers.Lenovo.toString(), 50);
	
	public static final ProductWarehouse product3 = new ProductWarehouse(1, "Laptop 300", 5000.00, Categories.Laptop.toString(), Providers.Lenovo.toString(), 2);
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Movements movements = new Movements();
		
		System.out.println("Tienda: "+ movements.addProduct(store, product1));
		System.out.println("Almacen: " + movements.addProduct(warehouse, product2));
		
		System.out.println("Nueva Solicitud: "+ movements.addProduct(newProducts, product1));
		
		System.out.println("Devoluci�n: "+ movements.refund(refund, product3, warehouse));
		
		System.out.println("Entrada de almac�n: "+ movements.warehouseMovementIn(store, 1, 3, warehouse));
		System.out.println("Salida de almac�n: "+ movements.warehouseMovementOut(store, 1, 3, warehouse));
		
		System.out.println("Tienda: " + movements.editName(store, 1, "Audifonos Bluetooth"));
		System.out.println("Tienda: " + movements.editPrice(store, 1, 4500.00));
		System.out.println("Tienda: " + movements.editCategory(store, 1, Categories.Audifonos.toString()));
		System.out.println("Tienda: " + movements.editProvider(store, 1, Providers.Razer.toString()));
		System.out.println("Tienda: " + movements.editQuantity(store, 1, 70));
		
		System.out.println("Almacen: " + movements.editName(store, 1, "Audifonos Bluetooth"));
		System.out.println("Almacen: " + movements.editPrice(store, 1, 4500.00));
		System.out.println("Almacen: " + movements.editCategory(store, 1, Categories.Audifonos.toString()));
		System.out.println("Almacen: " + movements.editProvider(store, 1, Providers.Razer.toString()));
		System.out.println("Almacen: " + movements.editQuantity(store, 1, 90));
		
		System.out.println("Tienda: " + movements.removeProduct(store, 1));
		System.out.println("Almacen: " + movements.removeProduct(warehouse, 1));
		
		

	}

}
